interface Translator {
    translate: () => string;
}

export default class PigLatinTraslator implements Translator {
    constructor(private string: string) {}

    public translate(): string {
        const words = this.string.split(/\s/);
        const translatedWords = words.map((word) => {
            return this.hasHyphen(word)
                ? this.translateWordWithHyphen(word)
                : this.translateWord(word);
        });

        return translatedWords.join(' ');
    }

    private translateWordWithHyphen(word: string): string {
        return word
            .split(/-/)
            .map((wordPart) => this.translateWord(wordPart))
            .join('-');
    }

    private translateWord(word: string): string {
        let wordChars = word.split('');
        const punctuationPosition = word.search(/[^\w]/i);
        const isCapitalFlags = wordChars.map((char) => this.isCapital(char));

        if (word.endsWith('way')) {
            return wordChars.join('');
        }

        wordChars.push(
            this.startsWithVowel(word) ? 'w' : wordChars.shift(),
            'a',
            'y'
        );

        if (punctuationPosition !== -1) {
            return this.resolvePunctuation(
                word,
                wordChars,
                punctuationPosition
            ).join('');
        }

        return this.capitalizeCharsByFlags(wordChars, isCapitalFlags).join('');
    }

    private resolvePunctuation(
        word: string,
        chars: Array<string>,
        punctuationPosition: number
    ): Array<string> {
        const punctuationSign = word[punctuationPosition];
        const isAtTheEnd = punctuationPosition === word.length - 1;
        const punctuationNewPosition = isAtTheEnd
            ? chars.length
            : punctuationPosition - word.length + 1;
        const charsWithoutPunctuation = chars.filter(
            (char) => char !== punctuationSign
        );
        const charsWithPunctuation = [
            ...charsWithoutPunctuation.slice(0, punctuationNewPosition),
            punctuationSign,
            ...charsWithoutPunctuation.slice(punctuationNewPosition),
        ];

        return charsWithPunctuation;
    }

    private capitalizeCharsByFlags(
        chars: Array<string>,
        flags: Array<boolean>
    ): Array<string> {
        return chars.map((char, i) =>
            this.capitalizeCharByFlag(char, flags[i])
        );
    }

    private capitalizeCharByFlag(char: string, isCapital: boolean): string {
        return isCapital ? char.toUpperCase() : char.toLowerCase();
    }

    private isCapital(char: string): boolean {
        return /[A-Z]/.test(char);
    }

    private hasHyphen(word: string): boolean {
        return /-/gi.test(word);
    }

    private startsWithVowel(word: string): boolean {
        return /^[a|e|i|o|u]/i.test(word);
    }
}
