import PigLatinTraslator from 'utils/PigLatinTraslator';

describe('PigLatinTraslator', (): void => {
    describe('translates samples correctly', () => {
        it(`Words that start with a consonant have their first 
          letter moved to the end of the word and the letters “ay”
          added to the end.`, (): void => {
            expect(new PigLatinTraslator('Hello').translate()).toBe('Ellohay');
        });

        it(`Words that start with a vowel have the letters “way”
          added to the end.`, (): void => {
            expect(new PigLatinTraslator('apple').translate()).toBe('appleway');
        });

        it(`Words that end in “way” are not modified.`, (): void => {
            expect(new PigLatinTraslator('stairway').translate()).toBe(
                'stairway'
            );
        });

        it(`Punctuation must remain in the same relative place
          from the end of the word.`, (): void => {
            expect(new PigLatinTraslator('can’t').translate()).toBe('antca’y');
            expect(new PigLatinTraslator('end.').translate()).toBe('endway.');
        });

        it(`Hyphens are treated as two words`, (): void => {
            expect(new PigLatinTraslator('this-thing').translate()).toBe(
                'histay-hingtay'
            );
        });

        it(`Capitalization must remain in the same place.`, (): void => {
            expect(new PigLatinTraslator('Beach').translate()).toBe('Eachbay');
            expect(new PigLatinTraslator('McCloud').translate()).toBe(
                'CcLoudmay'
            );
        });
    });
});
