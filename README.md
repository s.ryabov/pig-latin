## Pig Latin

Clone the repo and install dependencies:

```
npm install
```

Run the tests with:

```
npm t
```

Run tests in watch mode:

```
npm run test:watch
```
